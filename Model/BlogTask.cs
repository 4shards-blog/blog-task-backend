using System;

namespace BlogTaskBackend.Model
{
    public class BlogTask
    {
        public string Id { get; set; } = Guid.NewGuid().ToString().ToUpper();
        public string Summary { get; set; }
        public string Status { get; set; }
    }
}