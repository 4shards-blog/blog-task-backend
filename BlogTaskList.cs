using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BlogTaskBackend.Configuration;
using BlogTaskBackend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace BlogTaskBackend
{
    public class BlogTaskList
    {
        private readonly DatabaseOptions _databaseOptions;

        public BlogTaskList(IOptions<DatabaseOptions> databaseOptions)
        {
            _databaseOptions = databaseOptions.Value;
        }

        [FunctionName("BlogTaskList")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "tasks")] HttpRequestMessage req,
            ILogger log)
        {
            var result = new List<BlogTask>();
            MySqlSslMode.TryParse(_databaseOptions.SslMode, out MySqlSslMode sslMode);
            var builder = new MySqlConnectionStringBuilder
            {
                Server = _databaseOptions.Server,
                Database = _databaseOptions.Database,
                UserID = _databaseOptions.UserId,
                Password = _databaseOptions.Password,
                SslMode = sslMode,
            };

            await using var conn = new MySqlConnection(builder.ConnectionString);
            await conn.OpenAsync();
            await using var command = conn.CreateCommand();
            
            command.CommandText = @"SELECT id, summary, status FROM tasks";
            var reader = await command.ExecuteReaderAsync();
            while (await reader.ReadAsync())
            {
                result.Add(new BlogTask
                {
                    Id = reader.GetString(0),
                    Summary = reader.GetString(1),
                    Status = reader.GetString(2)
                });
            }
            return new OkObjectResult(result);
        }
    }
}
