using System.Net.Http;
using System.Threading.Tasks;
using BlogTaskBackend.Configuration;
using BlogTaskBackend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace BlogTaskBackend
{
    public class BlogTaskCreate
    {
        private readonly DatabaseOptions _databaseOptions;

        public BlogTaskCreate(IOptions<DatabaseOptions> databaseOptions)
        {
            _databaseOptions = databaseOptions.Value;
        }
        
        [FunctionName("BlogTaskCreate")]
        public async Task<IActionResult> Create(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "tasks")] HttpRequestMessage req,
            ILogger log)
        {
            var task = JsonConvert.DeserializeObject<BlogTask>(await req.Content.ReadAsStringAsync());
            MySqlSslMode.TryParse(_databaseOptions.SslMode, out MySqlSslMode sslMode);
            var builder = new MySqlConnectionStringBuilder
                        {
                            Server = _databaseOptions.Server,
                            Database = _databaseOptions.Database,
                            UserID = _databaseOptions.UserId,
                            Password = _databaseOptions.Password,
                            SslMode = sslMode,
                        };

            await using var conn = new MySqlConnection(builder.ConnectionString);
            await conn.OpenAsync();
            await using var command = conn.CreateCommand();

            command.CommandText = @"INSERT INTO tasks (id, summary) VALUES (@id, @summary);";
            command.Parameters.AddWithValue("@id", task.Id);
            command.Parameters.AddWithValue("@summary", task.Summary);
            var _ = await command.ExecuteNonQueryAsync();

            return new CreatedResult($"/api/tasks/{task.Id}", task);
        }
    }
}
