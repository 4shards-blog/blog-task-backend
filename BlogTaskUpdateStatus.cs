using System.Net.Http;
using System.Threading.Tasks;
using BlogTaskBackend.Configuration;
using BlogTaskBackend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace BlogTaskBackend
{
    public class BlogTaskUpdateStatus
    {
        private readonly DatabaseOptions _databaseOptions;

        public BlogTaskUpdateStatus(IOptions<DatabaseOptions> databaseOptions)
        {
            _databaseOptions = databaseOptions.Value;
        }
        
        [FunctionName("BlogTaskUpdateStatus")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "tasks/{id}")] HttpRequestMessage req,
            string id, ILogger log)
        {
            var task = JsonConvert.DeserializeObject<BlogTask>(await req.Content.ReadAsStringAsync());
            MySqlSslMode.TryParse(_databaseOptions.SslMode, out MySqlSslMode sslMode);
            var builder = new MySqlConnectionStringBuilder
            {
                Server = _databaseOptions.Server,
                Database = _databaseOptions.Database,
                UserID = _databaseOptions.UserId,
                Password = _databaseOptions.Password,
                SslMode = sslMode,
            };
            
            await using var conn = new MySqlConnection(builder.ConnectionString);
            await conn.OpenAsync();
            await using var command = conn.CreateCommand();

            command.CommandText = @"UPDATE tasks SET status = @status WHERE id = @id";
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@status", task.Status);
            var _ = await command.ExecuteNonQueryAsync();
            
            return new NoContentResult();
        }
    }
}
